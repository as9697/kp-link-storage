Za populacijo baze pri nalogi JdbcServlet zazeni:
```sql
CREATE TABLE uporabniki (
  id              serial NOT NULL PRIMARY KEY ,
  uporabnisko_ime varchar(30) NOT NULL,
  ime             varchar(40) NOT NULL,
  priimek         varchar(30) NOT NULL,
  email           varchar(40) UNIQUE
);

INSERT INTO uporabniki (uporabnisko_ime, ime, priimek, email) VALUES ('mirko7', 'Mirko', 'Novak', 'mirko@mir.ko');
```