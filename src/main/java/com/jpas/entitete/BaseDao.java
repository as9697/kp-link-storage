package com.jpas.entitete;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface BaseDao {
    Connection getConnection() throws NamingException, SQLException;

    Entiteta vrni(int id) throws NamingException;

    void vstavi(Entiteta ent) throws NamingException;

    void odstrani(int id) throws NamingException;

    void posodobi(Entiteta ent) throws NamingException;

    List<Entiteta> vrniVse() throws NamingException;
}