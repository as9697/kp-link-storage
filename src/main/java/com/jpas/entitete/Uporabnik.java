package com.jpas.entitete;

public class Uporabnik extends Entiteta {

    private String uporabniskoIme;
    private String ime;
    private String priimek;
    private String email;

    public Uporabnik(String uporabniskoIme, String ime, String priimek, String email) {
        this.uporabniskoIme = uporabniskoIme;
        this.ime = ime;
        this.priimek = priimek;
        this.email = email;
    }

    public Uporabnik(int id, String uporabniskoIme, String ime, String priimek, String email) {
        setId(id);
        this.uporabniskoIme = uporabniskoIme;
        this.ime = ime;
        this.priimek = priimek;
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format(
                "{" +
                        "\"id\":\"%s\"," +
                        "\"uporabniskoIme\":\"%s\"," +
                        "\"ime\":\"%s\"," +
                        "\"priimek\":\"%s\"," +
                        "\"email\":\"%s\"" +
                        "}", getId(), uporabniskoIme, ime, priimek, email);
    }

    public String getUporabniskoIme() {
        return uporabniskoIme;
    }

    public void setUporabniskoIme(String uporabniskoIme) {
        this.uporabniskoIme = uporabniskoIme;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}