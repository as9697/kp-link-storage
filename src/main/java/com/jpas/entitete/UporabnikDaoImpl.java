package com.jpas.entitete;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UporabnikDaoImpl implements BaseDao {


    private static final UporabnikDaoImpl uporabnikDao = new UporabnikDaoImpl();

    private Connection con;
    private final static Logger log = Logger.getLogger(UporabnikDaoImpl.class.getName());

    public static UporabnikDaoImpl getInstance() {
        return uporabnikDao;
    }

    @Override
    public Connection getConnection() throws NamingException, SQLException {
        InitialContext initCtx = new InitialContext();
        DataSource ds = (DataSource) initCtx.lookup("jdbc/SimpleJdbcDS");
        return ds.getConnection();
    }

    @Override
    public void vstavi(Entiteta ent) throws NamingException {
        PreparedStatement ps = null;

        try {
            if (con == null) {
                con = getConnection();
            }

            Uporabnik uporabnik = (Uporabnik) ent;

            String sql = "INSERT INTO uporabniki (uporabnisko_ime, ime, priimek, email) VALUES (?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setString(1, uporabnik.getUporabniskoIme());
            ps.setString(2, uporabnik.getIme());
            ps.setString(3, uporabnik.getPriimek());
            ps.setString(4, uporabnik.getEmail());
            ps.executeUpdate();

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
    }

    @Override
    public void odstrani(int id) throws NamingException {
        PreparedStatement ps = null;

        try {
            if (con == null) {
                con = getConnection();
            }

            String sql = "DELETE FROM uporabniki WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
    }

    @Override
    public void posodobi(Entiteta ent) throws NamingException {
        PreparedStatement ps = null;

        try {
            if (con == null) {
                con = getConnection();
            }

            Uporabnik uporabnik = (Uporabnik) ent;

            String sql = "UPDATE uporabniki SET uporabnisko_ime=? ime=? priimek=? email=? WHERE id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, uporabnik.getUporabniskoIme());
            ps.setString(2, uporabnik.getIme());
            ps.setString(3, uporabnik.getPriimek());
            ps.setString(4, uporabnik.getEmail());
            ps.executeUpdate();

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
    }

    @Override
    public List<Entiteta> vrniVse() throws NamingException {
        ArrayList<Entiteta> vsiUporabniki = new ArrayList<>();

        PreparedStatement ps = null;

        try {
            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabniki";

            ps = con.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()) {
                Uporabnik uporabnik = getUporabnikFromRS(resultSet);
                vsiUporabniki.add(uporabnik);
            }

            if (vsiUporabniki.isEmpty()) {
                log.info("Baza uporabnikov je prazna.");
            }

            return vsiUporabniki;
        }
        catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                }
                catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }

        return null;
    }

    private Uporabnik getUporabnikFromRS(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String ime = rs.getString("ime");
        String priimek = rs.getString("priimek");
        String uporabniskoIme = rs.getString("uporabnisko_ime");
        String email = rs.getString("email");

        return new Uporabnik(id, uporabniskoIme, ime, priimek, email);
    }

    @Override
    public Entiteta vrni(int id) throws NamingException {
        PreparedStatement ps = null;

        try {
            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabniki WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return getUporabnikFromRS(rs);
            } else {
                log.info("Uporabnik ne obstaja");
            }

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
        return null;
    }
}