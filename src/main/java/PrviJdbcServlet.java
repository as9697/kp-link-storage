import com.jpas.entitete.Entiteta;
import com.jpas.entitete.UporabnikDaoImpl;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@WebServlet("/servlet")
public class PrviJdbcServlet extends HttpServlet {

    private final static Logger log = Logger.getLogger(UporabnikDaoImpl.class.getName());


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UporabnikDaoImpl uporabnikDao = UporabnikDaoImpl.getInstance();

        // Pridobi vse uporabnike
        List<Entiteta> uporabniki;
        try {
            uporabniki = uporabnikDao.vrniVse();
            if (uporabniki == null) {
                throw new ServletException("Lista uporabnikov je NULL");
            }
        } catch (NamingException e) {
            e.printStackTrace();
            throw new ServletException("Napaka pri prodobivanju liste uporabnikov.");
        }

        // Sestavi string vseh uporabnikov
        String uporabnikiStr = "[";
        for (int i = 0; i < uporabniki.size(); i++) {
            uporabnikiStr += uporabniki.get(i).toString();
            if (i < uporabniki.size() - 1) {
                uporabnikiStr += ",";
            }
        }
        uporabnikiStr += "]";
        uporabnikiStr = "{\"Uporabniki\":" + uporabnikiStr + "}";

        // Izpisi uporabnike v konzolo
        log.info(uporabnikiStr);

        // Izpisi uporabnike v response
        resp.setContentType("application/javascript");
        resp.getWriter().write(uporabnikiStr);
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}