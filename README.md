# Run database
```bash
docker run -d --name link-storage -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=link-storage -p 5432:5432 postgres:10.5
```