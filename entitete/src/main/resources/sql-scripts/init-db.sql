INSERT INTO uporabnik (ime, priimek, uporabniskoIme, email) VALUES ('Janez', 'Novak', 'jnovak', 'janez@novak.si');
INSERT INTO uporabnik (ime, priimek, uporabniskoIme, email) VALUES ('Tina', 'Novak', 'tnovak', 'tina@novak.si');

INSERT INTO artikel (ime, link, uporabnik_id) VALUES ('krava', 'http://www.radio94.si/radio/content/uploads/2018/05/post/krava-1024x684.jpg', 1);
INSERT INTO artikel (ime, link, uporabnik_id) VALUES ('ovca', 'https://www.zurnal24.si/media/img/be/fa/cfea2956a6b6c3ae9ac0.jpeg', 1);
INSERT INTO artikel (ime, link, uporabnik_id) VALUES ('bambi', 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-20810-1eluhoz_ce3fa530.jpeg', 1);
INSERT INTO artikel (ime, link, uporabnik_id) VALUES ('riba', 'https://banjaluka.net/wp-content/uploads/2015/12/riba.jpg', 2);
