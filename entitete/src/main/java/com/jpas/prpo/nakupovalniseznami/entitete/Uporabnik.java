package com.jpas.prpo.nakupovalniseznami.entitete;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity(name = "uporabnik")

public class Uporabnik extends Entiteta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String ime;

    private String priimek;

    private String uporabniskoIme;

    @OneToMany(fetch=FetchType.LAZY, cascade=ALL, mappedBy="uporabnik")
    private List<Artikel> artikli;

    private String email;

    // getter in setter metode
    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getIme() {
        return ime;
    }

    public void setIme(String s) {
        ime = s;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String p) {
        priimek = p;
    }

    public String getUporabniskoIme() {
        return uporabniskoIme;
    }

    public void setUporabniskoIme(String u) {
        uporabniskoIme = u;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String e) {
        email = e;
    }

    public List<Artikel> getArtikli() {
        return artikli;
    }

    public void setArtikli(List<Artikel> artikli) {
        this.artikli = artikli;
    }
}

