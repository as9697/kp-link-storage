package com.jpas.prpo.nakupovalniseznami.entitete;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import javax.persistence.*;

@CascadeOnDelete
@Entity(name = "artikel")
public class Artikel extends Entiteta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String ime;

    private String link;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="uporabnik_id")
    private Uporabnik uporabnik;

    // getter in setter metode
    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getIme() {
        return ime;
    }

    public void setIme(String o) {
        ime = o;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Uporabnik getUporabnik() {
        return uporabnik;
    }

    public void setUporabnik(Uporabnik uporabnik) {
        this.uporabnik = uporabnik;
    }
}

