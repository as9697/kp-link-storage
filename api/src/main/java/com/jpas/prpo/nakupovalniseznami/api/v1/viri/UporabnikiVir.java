package com.jpas.prpo.nakupovalniseznami.api.v1.viri;

import com.jpas.prpo.nakupovalniseznami.api.v1.responses.ArtikelResp;
import com.jpas.prpo.nakupovalniseznami.api.v1.responses.UporabnikResp;
import com.jpas.prpo.nakupovalniseznami.entitete.Artikel;
import com.jpas.prpo.nakupovalniseznami.entitete.Uporabnik;
import com.jpas.prpo.nakupovalniseznami.storitve.UporabnikZrno;
import com.jpas.prpo.nakupovalniseznami.storitve.UstvariUporabnikaForm;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;

@ApplicationPath("/v1")
@Path("/uporabniki")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_HTML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@ApplicationScoped
public class UporabnikiVir {

    @Inject
    private UporabnikZrno uporabnikZrno;

    @Context
    protected UriInfo uriInfo;

    @PersistenceContext(unitName = "link-storage-jpa")
    private EntityManager em;

    @GET
    public Response vrniUporabnike(){
        try {
            QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
            List<Uporabnik> uporabniki = uporabnikZrno.pridobiUporabnike(query); // pridobi uporabnike
            Long count = JPAUtils.queryEntitiesCount(em, Uporabnik.class, query);

            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            ArrayList<UporabnikResp> uporabnikiResps = new ArrayList<>(uporabniki.size());

            for (Uporabnik u : uporabniki) {
                UporabnikResp uResp = new UporabnikResp();
                uResp.id = u.getId();
                uResp.ime = u.getIme();
                uResp.email = u.getEmail();
                uResp.priimek = u.getPriimek();
                uResp.uporabniskoIme = u.getUporabniskoIme();

                ArrayList<String> artikliLinki = new ArrayList<>(u.getArtikli().size());

                for (Artikel a : u.getArtikli()) {
                    artikliLinki.add(baseUri + "artikli/" + a.getId());
                }
                uResp.artikli = artikliLinki;

                uporabnikiResps.add(uResp);

            }
            GenericEntity<List<UporabnikResp>> genericEntity = new GenericEntity<List<UporabnikResp>> (uporabnikiResps) {};

            return Response.status(Response.Status.OK).entity(genericEntity)
                    .encoding("utf-8")
                    .header("X-Total-Count", count)
                    .build();

        } catch (Exception e){
            System.out.println("EXCEPTION:");
            System.out.println(e);
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Path("{id}")
    @GET
    public Response vrniUporabnika(@PathParam("id") Integer id){
        Uporabnik u = uporabnikZrno.pridobiUporabnika(id);

        if (u == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            UporabnikResp uResp = new UporabnikResp();
            uResp.id = u.getId();
            uResp.ime = u.getIme();
            uResp.email = u.getEmail();
            uResp.priimek = u.getPriimek();
            uResp.uporabniskoIme = u.getUporabniskoIme();

            ArrayList<String> artikliLinki = new ArrayList<>(u.getArtikli().size());

            for (Artikel a : u.getArtikli()) {
                artikliLinki.add(baseUri + "artikli/" + a.getId());
            }
            uResp.artikli = artikliLinki;

            return Response.status(Response.Status.OK).entity(uResp).build();
        }
    }

    @POST
    public Response ustvariUporabnika(UstvariUporabnikaForm form) {

        Uporabnik u = new Uporabnik();
        u.setId(form.getId());
        u.setUporabniskoIme(form.getUporabniskoIme());
        u.setEmail(form.getEmail());
        u.setIme(form.getIme());
        u.setPriimek(form.getPriimek());

        try {
            uporabnikZrno.ustvariUporabnika(u);

            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            UporabnikResp uResp = new UporabnikResp();
            uResp.id = u.getId();
            uResp.ime = u.getIme();
            uResp.email = u.getEmail();
            uResp.priimek = u.getPriimek();
            uResp.uporabniskoIme = u.getUporabniskoIme();

            ArrayList<String> artikliLinki = new ArrayList<>(u.getArtikli().size());

            for (Artikel a : u.getArtikli()) {
                artikliLinki.add(baseUri + "artikli/" + a.getId());
            }
            uResp.artikli = artikliLinki;

            return Response.status(Response.Status.CREATED).entity(uResp).build();

        } catch (Exception e){
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @Path("{id}")
    @DELETE
    public Response odstraniUporabnika(@PathParam("id") Integer id) {

        try {
            uporabnikZrno.odstraniUporabnika(id);
            return Response.status(Response.Status.NO_CONTENT).build();

        } catch (Exception e){
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }

}