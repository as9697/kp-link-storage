package com.jpas.prpo.nakupovalniseznami.api.v1;

import com.jpas.prpo.nakupovalniseznami.api.v1.responses.CustomMessageBodyWritter;
import com.jpas.prpo.nakupovalniseznami.api.v1.viri.*;

import javax.ws.rs.ApplicationPath;
import java.util.Set;

@ApplicationPath("v1")
public class RestStoritve extends javax.ws.rs.core.Application {

    @Override
    public Set<Class<?>> getClasses() {

        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(UporabnikiVir.class);
        resources.add(ArtikliVir.class);
        resources.add(CustomMessageBodyWritter.class);

        return resources;

    }

}
