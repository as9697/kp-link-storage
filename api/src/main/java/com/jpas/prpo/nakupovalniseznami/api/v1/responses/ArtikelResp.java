package com.jpas.prpo.nakupovalniseznami.api.v1.responses;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArtikelResp {
    public Integer id;

    public String ime;

    public String link;

    public String uporabnik;
}
