package com.jpas.prpo.nakupovalniseznami.api.v1.responses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UporabnikResp {
    public Integer id;

    public String ime;

    public String priimek;

    public String uporabniskoIme;

    public List<String> artikli;

    public String email;
}
