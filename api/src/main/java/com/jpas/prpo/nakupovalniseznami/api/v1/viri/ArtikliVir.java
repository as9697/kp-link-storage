package com.jpas.prpo.nakupovalniseznami.api.v1.viri;

import com.jpas.prpo.nakupovalniseznami.api.v1.responses.ArtikelResp;
import com.jpas.prpo.nakupovalniseznami.entitete.Artikel;
import com.jpas.prpo.nakupovalniseznami.entitete.Uporabnik;
import com.jpas.prpo.nakupovalniseznami.storitve.ArtikelZrno;
import com.jpas.prpo.nakupovalniseznami.storitve.UporabnikZrno;
import com.jpas.prpo.nakupovalniseznami.storitve.UstvariArtikelForm;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;

@Path("artikli")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_HTML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@ApplicationScoped
public class ArtikliVir {

    @Inject
    private ArtikelZrno artikelZrno;

    @Inject
    private UporabnikZrno uporabnikZrno;

    @Context
    protected UriInfo uriInfo;

    @PersistenceContext(unitName = "link-storage-jpa")
    private EntityManager em;

    @GET
    public Response vrniArtikle() {

        try {
            QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
            List<Artikel> artikli = artikelZrno.pridobiArtikle(query);
            Long count = JPAUtils.queryEntitiesCount(em, Artikel.class, query);

            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            ArrayList<ArtikelResp> artikliResp = new ArrayList<>(artikli.size());

            for (Artikel a : artikli) {
                ArtikelResp aResp = new ArtikelResp();
                aResp.id = a.getId();
                aResp.ime = a.getIme();
                aResp.link = a.getLink();
                aResp.uporabnik = baseUri + "uporabniki/" + a.getUporabnik().getId();

                artikliResp.add(aResp);

            }

            GenericEntity<List<ArtikelResp>> genericEntity = new GenericEntity<List<ArtikelResp>> (artikliResp) {};

            return Response.status(Response.Status.OK).entity(genericEntity)
                    .header("X-Total-Count", count)
                    .build();

        } catch (Exception e){
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }

    @Path("{id}")
    @GET
    public Response vrniArtikel(@PathParam("id") Integer id){
        Artikel artikel = artikelZrno.pridobiArtikel(id);

        if (artikel == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            ArtikelResp aResp = new ArtikelResp();
            aResp.id = artikel.getId();
            aResp.ime = artikel.getIme();
            aResp.link = artikel.getLink();
            aResp.uporabnik = baseUri + "uporabniki/" + artikel.getUporabnik().getId();

            return Response.status(Response.Status.OK).entity(aResp).build();
        }
    }

    @POST
    public Response ustvariArtikel(UstvariArtikelForm form) {

        Artikel artikel = new Artikel();
        artikel.setLink(form.getLink());
        artikel.setIme(form.getIme());

        Integer uporabnikId = Integer.parseInt(form.getUporabnik_id());

        Uporabnik u = uporabnikZrno.pridobiUporabnika(uporabnikId);

        artikel.setUporabnik(u);

        try {
            artikelZrno.ustvariArtikel(artikel);

            String baseUri = uriInfo.getBaseUri().toString();
            baseUri = baseUri.replace("http:", "https:");

            ArtikelResp aResp = new ArtikelResp();
            aResp.id = artikel.getId();
            aResp.ime = artikel.getIme();
            aResp.link = artikel.getLink();
            aResp.uporabnik = baseUri + "uporabniki/" + artikel.getUporabnik().getId();

            return Response.status(Response.Status.CREATED).entity(aResp).build();

        } catch (Exception e){
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

//    @Path("{id}")
//    @DELETE
//    public Response odstraniArtikel(@PathParam("id") Integer id) {
//
//        try {
//            artikelZrno.odstraniArtikel(id);
//            return Response.status(Response.Status.NO_CONTENT).build();
//
//        } catch (Exception e){
//            e.printStackTrace();
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//
//    }

}