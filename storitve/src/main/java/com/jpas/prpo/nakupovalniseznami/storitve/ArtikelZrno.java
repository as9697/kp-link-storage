package com.jpas.prpo.nakupovalniseznami.storitve;


import com.jpas.prpo.nakupovalniseznami.entitete.Artikel;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class ArtikelZrno {

    private Logger log = Logger.getLogger(ArtikelZrno.class.getName());

    @PersistenceContext(unitName = "link-storage-jpa")
    private EntityManager em;

    public List<Artikel> pridobiArtikle(QueryParameters query) {
        return JPAUtils.queryEntities(em, Artikel.class, query);
    }

    public Artikel pridobiArtikel(int id) {
        Artikel artikel = em.find(Artikel.class, id);

        if (artikel == null) {
            log.warning("artikel z id " + id + " ne obstaja");
        }

        return artikel;
    }

    @Transactional
    public void odstraniArtikel(int id) {
        Artikel artikel = pridobiArtikel(id);

        if (artikel == null) {
            log.warning("artikel z id " + id + " ne obstaja");
        }

        em.remove(artikel);
        em.flush();
    }

    @Transactional
    public void ustvariArtikel(Artikel artikel) {
        if (artikel == null) {
            log.warning("artikel je null");
        }

        em.persist(artikel);
        em.flush();
    }

    @Transactional
    public void posodobiArtikel(int id, Artikel artikel) throws EntityNotFoundException {
        if (artikel == null) {
            log.warning("podani artikel je null");
        }

        Artikel shranjenArtikel = pridobiArtikel(id);

        if (shranjenArtikel == null) {
            log.warning("uporabnikseznam z id " + id + " ne obstaja");
            throw new EntityNotFoundException("Id artikla: " + id);
        }

        em.merge(artikel);
        em.flush();
    }
}