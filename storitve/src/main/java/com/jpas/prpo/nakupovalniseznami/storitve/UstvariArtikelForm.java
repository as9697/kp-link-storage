package com.jpas.prpo.nakupovalniseznami.storitve;

public class UstvariArtikelForm {

    private String link;
    private String ime;
    private String uporabnik_id;

    public String getUporabnik_id() {
        return uporabnik_id;
    }

    public void setUporabnik_id(String uporabnik_id) {
        this.uporabnik_id = uporabnik_id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
