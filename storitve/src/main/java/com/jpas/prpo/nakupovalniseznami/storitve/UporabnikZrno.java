package com.jpas.prpo.nakupovalniseznami.storitve;

import com.jpas.prpo.nakupovalniseznami.entitete.Uporabnik;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class UporabnikZrno {

    private Logger log = Logger.getLogger(UporabnikZrno.class.getName());

    @PersistenceContext(unitName = "link-storage-jpa")
    private EntityManager em;

    public List<Uporabnik> pridobiUporabnike(QueryParameters query) {
        return JPAUtils.queryEntities(em, Uporabnik.class, query);
    }

    public Uporabnik pridobiUporabnika(int id) {
        Uporabnik uporabnik = em.find(Uporabnik.class, id);

        if (uporabnik == null) {
            log.warning("uporabnik z id " + id + " ne obstaja");
        }

        return uporabnik;
    }

    @Transactional
    public void odstraniUporabnika(int id) {
        Uporabnik uporabnik = pridobiUporabnika(id);

        if (uporabnik == null) {
            log.warning("uporabnik z id " + id + " ne obstaja");
        }

        em.remove(uporabnik);
        em.flush();
    }

    @Transactional
    public void ustvariUporabnika(Uporabnik uporabnik) {
        if (uporabnik == null) {
            log.warning("podani uporabnik je null");
            return;
        }

        em.persist(uporabnik);
        em.flush();
    }

    @Transactional
    public void posodobiUporabnika(int id, Uporabnik uporabnik) throws EntityNotFoundException {
        if (uporabnik == null) {
            log.warning("podani uporabnik je null");
        }
        Uporabnik shranjenUporabnik = pridobiUporabnika(id);

        if (shranjenUporabnik == null) {
            log.warning("uporabnik z id " + id + " ne obstaja");
            throw new EntityNotFoundException("Id uporabnika: " + id);
        }

        em.merge(uporabnik);
        em.flush();
    }
}